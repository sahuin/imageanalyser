# Read an image and call an API to get details of the image

#import Requests library to make REST API calls
import requests

#import JSON library
import json


######
#API Details
#API Documentation URL
#https://westus.dev.cognitive.microsoft.com/docs/services/56f91f2d778daf23d8ec6739/operations/56f91f2e778daf14a499e1fa
azure_vision_service_url = 'https://imageanalyserapi.cognitiveservices.azure.com/vision/v1.0/analyze'
SUBSCRIPTION_KEY = 'XXXXXXXXXX'
request_header = {
    "Content-Type":"application/octet-stream",
    "Ocp-Apim-Subscription-Key": SUBSCRIPTION_KEY
}
parameters = {
    'visualFeatures':'Description,Tags',
    'language':'en'
}
#####
# Image details

image_path = "./data/image1.jpeg"
image = open(image_path,'rb')
image_data = image.read()
image.close()

#########################


request_response = requests.post(azure_vision_service_url,headers=request_header, params= parameters, data=image_data)
request_response.raise_for_status()

results = request_response.json()


image_title = results["description"]["captions"][0]["text"]
print(image_title)